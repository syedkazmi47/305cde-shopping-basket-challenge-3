// This is the basket module!
(function (window) {

    // Array to store the items in the basket
    var storeItems = [];

    // Array to store the price of all items in the basket
    var priceOfItems = [];

    //Total items in the basket
    var totalItems = 0;

    // display area
    var output = '';

    function updateView(){
        
         // below code outputs data onto the screen
        for (var i = 0; i < storeItems.length; i++) 
            {
                //output  +=  "<p id='itemTitle'>" + storeItems[i].title + " " + "\n" + "</p>";
                output += "<p class='item' value='" + i + "'>" + storeItems[i].title + " " + "\n" + "</p>";
            }
        var mainBasket = document.getElementById("mainBasket");
        var innerBasket = document.getElementById("innerBasket");
        var cost = document.getElementById("basketTitle");
        
        cost.innerHTML = "Your Basket " + " | "+ "Total Cost:  \n" + "£" +priceOfItems.reduce(addPrice, 0); 
        innerBasket.innerHTML = output;
        output = "";
    
    }
    
    // Closure to Count the total items in the basket
    var itemCounter = function () {
        totalItems++;
        console.log(totalItems);
        return totalItems;
    }
    
    // Closure to save basket items in the array
    var saveData = function (a) {
        
        var innerBasket = document.getElementById("innerBasket");
        innerBasket.addEventListener('click', function(e) { removedata(e);});
        
        storeItems.push(a);
        priceOfItems.push(a.price);

        console.log(storeItems);
        updateView();
    }
    
    
    function removedata(e){
       
        
        var index = e.target.getAttribute('value');
        var e = document.getElementById('basketShow');
        
        storeItems.splice(index, 1);
        priceOfItems.splice(index,1);
        
        e.innerHTML = storeItems.length;
        // re-display the records from storeItems.
        updateView();
    }
    
    // function to close the basket
    var closeBasket = function (){
        
        document.getElementById("mainBasket").style.display = "none";
    }
    

    // Closure to display the basket state
    var showBasket = function () 
    {
        
        mainBasket.style.display = "block";
        
    }
    
    function addPrice(a, b) {
        return a + Math.round(b);
    }
    
    var clearBasket = function()
    {
        storeItems = [];
        priceOfItems = [];
        totalItems = 0;
        console.log(storeItems);
        console.log(priceOfItems);
        updateView();
        return totalItems;
    }


    window.basket = window.basket || {};
    window.basket.itemCounter = itemCounter;
    window.basket.showBasket = showBasket;
    window.basket.saveData = saveData;
    window.basket.closeBasket = closeBasket;
    window.basket.clearBasket = clearBasket;
   
})(window);