    var ans = '';

    // Constructor to create new Items
    function Item(title, description, price, btn, image) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.image = image;
        this.btn = btn;
        this.sayName(this);
    };

    // Prototype to complete two tasks:
    // 1) Create and attach buttons to each object
    // 2) Create an image for each object
    Item.prototype.sayName = function (item) {
        
            var ItemDiv = document.getElementById('itemsList');
            var bask = document.getElementById('buttonsList');
            var basketIcon = document.getElementById('basketIcon');

            // Creating an image for each object
            var x = document.createElement("IMG");
            x.id = "test"
            // Setting the source for each image
            x.setAttribute("src", this.image);
            
            // Setting attributes
            x.setAttribute("width", "200");
            x.setAttribute("height", "240");


            // Creating and attached button to each object
            this.btn = document.createElement("INPUT");
            this.btn.setAttribute("type", "button");
            this.btn.setAttribute("value", "Add to basket");
        
            // Styling the button
            this.btn.setAttribute("style", "width: 200px; height: 45px; background-color:#27ae60; margin-left: 700px; color: #fff; font-family:'Source Sans Pro'; font-size: 13px; top: -60px; position: relative; text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25); border: 0; border-bottom: 2px solid #27ae60; -webkit-box-shadow: inset 0 -2px #27ae60; box-shadow: inset 0 -2px #27ae60;");

            // Adding an on drag listener to the image
            x.addEventListener("dragstart", function(){myBasket(item);});
          // Adding an click event Listener for each button
            this.btn.addEventListener("click", function () {myBasket(item);});

            var myDiv = document.createElement("div");
            myDiv.setAttribute("style", "border-bottom: 1px solid #f1c40f; width: 400px; margin-left: 250px; margin-top: -300px; padding: 15px;");

            ans = '<h1>' + this.title + '</h1>';
            ans += '<p>' + "Description: " + this.description + "\n" + '</p>';
            ans += '<p>' + "Price: £" + this.price + "\n" + '</p>';

            // display the object data    
            ItemDiv.appendChild(x);
            myDiv.innerHTML = ans;

            ItemDiv.appendChild(myDiv);

            // append the button to the data div
            ItemDiv.appendChild(this.btn);
        };

        // Event Listener for each button 
        function myBasket(item) 
        {
            // Invoking a method to send ans save clicked item to the basket module
            basket.saveData(item);
            
            // Invoking a method to count total items in basket and then update the DOM
            var e = document.getElementById('basketShow');
            e.innerHTML = basket.itemCounter();
        }

        // function to display the basket state and the data within
        function displayBasket() {
            basket.showBasket();
        }

        function closeB(){
            basket.closeBasket();
        }

        function clearBaskt(){
            basket.clearBasket();
            var e = document.getElementById('basketShow');
            e.innerHTML = basket.clearBasket();
        }

        window.onload = displayInfo;

        // function runs on window onload and creates 4 new items using the "new" keyword
        function displayInfo() {
            var myItem = new Item("JavaScript: The Good Parts", "Most programming languages contain good and bad parts, but JavaScript has more than its share of the bad, having been developed and released in a hurry before it could be refined.", 29, "", "images/img1.png");

            var myItem2 = new Item("AngularJS", "Develop smaller, lighter web apps that are simple to create and easy to test, extend, and maintain as they grow. This hands-on guide introduces you to AngularJS, the open source JavaScript framework that uses Model–view–controller (MVC) architecture.", 39, "", "images/angularjs.png");

            var myItem3 = new Item("Node.js the Right Way", "Get to the forefront of server-side JavaScript programming by writing compact, robust, fast, networked Node applications that scale. Ready to take JavaScript beyond the browser, explore dynamic languages features and embrace evented programming?", 100, "", "images/jwnode.png");

            var myItem4 = new Item("MongoDB: The Definitive Guide", "Manage the huMONGOus amount of data collected through your web application with MongoDB. This authoritative introduction—written by a core contributor to the project—shows you the many advantages of using document-oriented databases", 100, "", "images/mongodb.png");
        }